
# docker-webtop-archimate

## Why

This image, based on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop), 
provides [the Archimate tool](https://www.archimatetool.com).

## Details

- See details on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop).
- See available branches.


